# yml2drf - YAML to Django Rest Framework

yml2drf is a tool for automating Django Rest Framework code generation.

Specify your models in a yml file, and yml2drf will generate the models, 
serializers, and views files for you.

## Usage

### Yaml file structure

```
models:
  Animal:
    - name:CharField
    - birthday:DateField
```

It is also possible to add invisible attributes. 
These attributes will be included in models but excluded
from serializers and views.

```
models:
  Animal:
    - name:CharField
    - birthday:DateField
    - secret:CharField:invisible
```

### Generate python files
```
yml2drf generate-models path_to_your_yml > models.py

yml2drf generate-serializers path_to_your_yml > serializers.py

yml2drf generate-views path_to_your_yml > views.py
```