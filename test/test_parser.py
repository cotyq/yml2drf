# This file is part of the yml2drf Project (https://gitlab.com/cotyq/yml2drf).
# Copyright (c) 2021, Constanza Quaglia
# License: MIT
# Full Text: https://gitlab.com/cotyq/yml2drf/-/blob/master/LICENSE

from yml2drf.parser import Parser


def test_parse_two_classes():
    test_file_path = "test/yml/two_classes.yml"
    with open(test_file_path) as test_file:
        models = Parser.yml_to_model(test_file)
        for cls, attrs in models.items():
            assert isinstance(attrs, list)
            assert len(attrs) > 0
            assert all([isinstance(attr, tuple) for attr in attrs])
