# This file is part of the yml2drf Project (https://gitlab.com/cotyq/yml2drf).
# Copyright (c) 2021, Constanza Quaglia
# License: MIT
# Full Text: https://gitlab.com/cotyq/yml2drf/-/blob/master/LICENSE

import pytest


def test_generate_model(script_runner):
    test_file = "test/yml/two_classes.yml"
    ret = script_runner.run("yml2drf", "generate-models", test_file)

    assert ret.success
    template = ret.stdout
    try:
        compile(template, "<string>", "exec")
    except SyntaxError:
        pytest.fail("Syntax error in model generation with two classes")


def test_generate_model_bad_format(script_runner):
    test_file = "test/yml/bad_format.yml"
    ret = script_runner.run("yml2drf", "generate-models", test_file)

    assert not ret.success
    assert ret.stdout == ""
    assert len(ret.stderr) > 0


def test_generate_model_invalid_class_name(script_runner):
    test_file = "test/yml/invalid_class_names.yml"
    ret = script_runner.run("yml2drf", "generate-models", test_file)

    assert not ret.success
    assert ret.stdout == ""
    assert len(ret.stderr) > 0


def test_generate_model_not_yml(script_runner):
    test_file = "test/yml/not_yml.yml"
    ret = script_runner.run("yml2drf", "generate-models", test_file)

    assert not ret.success
    assert ret.stdout == ""
    assert len(ret.stderr) > 0


def test_generate_serializer(script_runner):
    test_file = "test/yml/serializable.yml"
    ret = script_runner.run("yml2drf", "generate-serializers", test_file)

    assert ret.success
    template = ret.stdout
    try:
        compile(template, "<string>", "exec")
    except SyntaxError:
        pytest.fail("Syntax error in serializer generation")


def test_generate_views(script_runner):
    test_file = "test/yml/serializable.yml"
    ret = script_runner.run("yml2drf", "generate-views", test_file)

    assert ret.success
    template = ret.stdout
    try:
        compile(template, "<string>", "exec")
    except SyntaxError:
        pytest.fail("Syntax error in views generation")
