# This file is part of the yml2drf Project (https://gitlab.com/cotyq/yml2drf).
# Copyright (c) 2021, Constanza Quaglia
# License: MIT
# Full Text: https://gitlab.com/cotyq/yml2drf/-/blob/master/LICENSE

"""Template Generator Module."""
import abc

from jinja2 import Template


MODEL_TEMPLATE = Template(
    """from django.db import models


{% for c, attrs in model.items() %}
class {{c}}(models.Model):
{%- for attr in attrs %}
    {{attr.name}}=models.{{attr.type}}()
{%- endfor %}

{% endfor %}

"""
)


SERIALIZER_TEMPLATE = Template(
    """from .models import {{ model.keys() | join(', ')}}
from rest_framework import serializers


{% for c, attrs in model.items() %}
class {{c}}Serializer(serializers.ModelSerializer):
    class Meta:
        model = {{c}}
        fields = ['{{ attrs
            | selectattr("visible")
            | join("', '", attribute='name') }}']

{% endfor %}

"""
)

VIEWS_TEMPLATE = Template(
    """from .models import {{ model.keys() | join(', ')}}
from .serializers import {{ serializers | join(', ') }}
from rest_framework import viewsets


{% for c, attrs in model.items() %}
class {{c}}ViewSet(viewsets.ModelViewSet):
    queryset = {{c}}.objects.all()
    serializer_class = {{c}}Serializer

{% endfor %}

"""
)


class TemplateGenerator(abc.ABC):
    """Class to generate template file for Django REST Framework."""

    @staticmethod
    def generate_models(model):
        """Generate the template for models according to the model received.

        :param model: models defined in yml
        :return: string with the generated template
        """
        return TemplateGenerator._generate_template(
            MODEL_TEMPLATE, model=model
        )

    @staticmethod
    def generate_serializers(model):
        """Generate the template for serializers according to the model received.

        :param model: models defined in yml
        :return: string with the generated template
        """
        return TemplateGenerator._generate_template(
            SERIALIZER_TEMPLATE, model=model
        )

    @staticmethod
    def generate_views(model):
        """Generate the template for views according to the model received.

        :param model: models defined in yml
        :return: string with the generated template
        """
        serializers = [f"{c}Serializer" for c in model.keys()]
        tpl = TemplateGenerator._generate_template(
            VIEWS_TEMPLATE, model=model, serializers=serializers
        )
        return tpl

    @staticmethod
    def _generate_template(template, **kwargs):
        """Generate the template according to the model and base template received.

        :param models: models defined in yml
        :param template: base template to fill with the models
        :return: string with the generated template
        """
        tpl = template.render(**kwargs)
        return tpl
