# This file is part of the yml2drf Project (https://gitlab.com/cotyq/yml2drf).
# Copyright (c) 2021, Constanza Quaglia
# License: MIT
# Full Text: https://gitlab.com/cotyq/yml2drf/-/blob/master/LICENSE

"""yml2drf is a tool for automating Django Rest Framework code generation."""
