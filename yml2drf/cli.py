# This file is part of the yml2drf Project (https://gitlab.com/cotyq/yml2drf).
# Copyright (c) 2021, Constanza Quaglia
# License: MIT
# Full Text: https://gitlab.com/cotyq/yml2drf/-/blob/master/LICENSE

"""yml2drf Command Line Interface."""

import typer

from .parser import Parser
from .template import TemplateGenerator


class CLI:
    """yml2drf console client."""

    app = typer.Typer()

    footnotes = "\n".join(
        [
            "MIT License.",
            "Copyright (c) 2021.",
            "Constanza Quaglia.",
        ]
    )

    @staticmethod
    @app.command()
    def version():
        """Print yml2drf version."""
        print("1.0")

    @staticmethod
    @app.command()
    def generate_models(input_file):
        """Generate models.py file from input yaml.

        :param input_file: Path to input yml file
        """
        with open(input_file) as input_yml:
            models = Parser.yml_to_model(input_yml)
            template = TemplateGenerator.generate_models(models)
            try:
                compile(template, "<string>", "exec")
            except SyntaxError:
                raise typer.Exit("Invalid class or attributes names")
            print(template)

    @staticmethod
    @app.command()
    def generate_serializers(input_file):
        """Generate serializers.py file from input yaml.

        :param input_file: Path to input yml file
        """
        with open(input_file) as input_yml:
            models = Parser.yml_to_model(input_yml)
            template = TemplateGenerator.generate_serializers(models)
            try:
                compile(template, "<string>", "exec")
            except SyntaxError:
                raise typer.Exit("Invalid class or attributes names")
            print(template)

    @staticmethod
    @app.command()
    def generate_views(input_file):
        """Generate views.py file from input yaml.

        :param input_file: Path to input yml file
        """
        with open(input_file) as input_yml:
            models = Parser.yml_to_model(input_yml)
            template = TemplateGenerator.generate_views(models)
            try:
                compile(template, "<string>", "exec")
            except SyntaxError:
                raise typer.Exit("Invalid class or attributes names")
            print(template)


def main():
    """Run the yml2drf CLI interface."""
    cli = CLI()
    cli.app()


if __name__ == "__main__":
    main()
