# This file is part of the yml2drf Project (https://gitlab.com/cotyq/yml2drf).
# Copyright (c) 2021, Constanza Quaglia
# License: MIT
# Full Text: https://gitlab.com/cotyq/yml2drf/-/blob/master/LICENSE

"""YML files Parser Module."""

from collections import namedtuple

import yaml

REQUIRED_KEYS = {"models"}


class Parser:
    """Parse user input files."""

    @staticmethod
    def yml_to_model(yml_file):
        """Parse yml input files to dictionary.

        The generated dictionary contains a key for each
        defined class and a ModelAttribute array as value.

        :param yml_file: YML file object
        :return: dictionary filled with the file content
        """
        ModelAttribute = namedtuple(
            "ModelAttribute", ["name", "type", "visible"]
        )
        data = yaml.load(yml_file, Loader=yaml.Loader)
        keys = data.keys()

        if not REQUIRED_KEYS.issubset(keys):
            raise SyntaxError("Wrong formatted yml")

        models = data["models"]
        for cls, attrs in models.items():
            models[cls] = []
            for attr in attrs:
                split_attr = attr.split(":")
                if len(split_attr) < 2:
                    raise SyntaxError("Wrong formatted yml")
                visible = "invisible" not in split_attr[2:]
                model_attr = ModelAttribute(
                    split_attr[0], split_attr[1], visible
                )
                models[cls].append(model_attr)
        return models
