import os
import pathlib

from setuptools import find_packages, setup

REQUIREMENTS = ["Jinja2", "typer", "pyyaml"]

PATH = pathlib.Path(os.path.abspath(os.path.dirname(__file__)))

DESCRIPTION = (
    "yml2drf is a tool for automating Django Rest Framework code generation"
)

with open(PATH / "README.md") as fp:
    LONG_DESCRIPTION = fp.read()

setup(
    name="yml2drf",
    version="1.0",
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    author=["Constanza Quaglia"],
    author_email="cotii.q@gmail.com",
    license="MIT",
    keywords=["yml2drf"],
    packages=find_packages(include=["yml2drf", "yml2drf.*"]),
    install_requires=REQUIREMENTS,
    include_package_data=True,
    entry_points={"console_scripts": ["yml2drf=yml2drf.cli:main"]},
)
